import React from 'react';
import './App.css';
import ButtonComponent from "./ButtonComponent";
import DisplayComponent from "./comps/DisplayComponent";
import CounterContext from "./CounterContext";

export interface AppState {
    counter: number;
    setCounter: any;
}


class App extends React.Component<{}, AppState> {
    constructor(props: {}) {
        super(props);
        this.state = {
            counter: 0,
            setCounter: this.setCounter
        }
    }

    setCounter = (x: number) => {
        this.setState({
            counter: x
        })
    }

    render() {
        return (
            <div className="App">
                <CounterContext.Provider value={this.state}>
                    <DisplayComponent/>
                    <ButtonComponent/>

                </CounterContext.Provider>
            </div>
        );
    }
}

export default App;
