import React from 'react';
import CounterContext from "../CounterContext";

export class DisplayComponent extends React.Component {
    render() {
        return (
            <div>
                <CounterContext.Consumer>
                    {
                        ({counter}) => (
                            <h1>{counter}</h1>
                        )
                    }
                </CounterContext.Consumer>
            </div>
        );
    }
}

export default DisplayComponent;
