import React from 'react';
import CounterContext from "./CounterContext";

// export interface ButtonComponentProps {
//     propVal: string;
// }

export class ButtonComponent extends React.Component<{}, {}> {
    render() {
        return (
            <div>
                <CounterContext.Consumer>
                    {
                        ({counter, setCounter})=> (
                            <div>
                            <button onClick={() => setCounter(counter +1)}>PLUS</button>
                            <button onClick={() => setCounter(counter -1)}>MINUS</button>
                            </div>
                        )
                    }
                </CounterContext.Consumer>
            </div>
        );
    }
}

export default ButtonComponent;
